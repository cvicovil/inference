# HH physics models

- `hh_model.py`: The default HH physics model covering ggF, VBF and VHH production. Self-contained file including all necessary objects.
- `hh_model_C2klkt.py`: Extension of the HH physics model to include C2 in the modeling of ggF production (in addition to kl and kt). Requires `hh_model.py`.
- `HBRscaler.py`: Initial implementation of the scaling of Higgs branching ratios and single Higgs backgrounds with kappa parameters. Fully included in `hh_model.py`.
- `HHModelPinv.py` (*deprecated*): Old HH physics model for development. To be removed.
- `HHModelPinv_C2klkt.py` (*deprecated*): Old HH physics model for development. To be removed.
- `HHModelPinvVHH.py` (*deprecated*): Old HH physics model for development. To be removed.
