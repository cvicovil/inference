The `PlotEFTUpperLimits` task collects the results from the `MergeEFTUpperLimits` task and visualizes them in a plot.

<div class="dhi_parameter_table">

--8<-- "content/snippets/parameters.md@-2,21,61,48,17,18,10,11,3,4,7,8,9"

</div>
